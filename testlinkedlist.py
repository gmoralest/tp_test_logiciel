#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Creation and manipulation of a Linked list from a txt file.
"""

__author__ = ' Gabriel Morales'

import unittest
from POO import LinkedList



# TEST_4. le sommet d’une pile sur laquelle on vient d’empiler un élément e est e.


class TestLinkedList(unittest.TestCase):
    """ Class containing all the test for the POO.py script"""

    def setUp(self):
        self.test_list = LinkedList()


    def test_empty_head_node(self):
        """TEST_1. une pile venant d’être créée est vide"""

        self.assertIsNone(self.test_list.head)

    def test_new_node(self):
        """TEST_2. toute pile sur laquelle on vient d’empiler un élément est non vide"""

        self.test_list.insert_at_begining(3)
        self.assertIsNotNone(self.test_list.head)

    def test_node_over_node_end(self):
        """TEST_3A. une pile qui subit un empilement suivi d’un dépilement est inchangée"""

        data_list = [22, 33, 44]
        self.test_list.insert_values(data_list)
        self.assertEqual(data_list[0], self.test_list.head.data)

    def test_node_over_node_begining(self):
        """TEST_3B. une pile qui subit un empilement suivi d’un dépilement est inchangée"""

        data_list = [22, 33, 44]
        self.test_list.insert_values2(data_list)
        itr = self.test_list.head
        while itr.next:
            itr = itr.next
        self.assertEqual(data_list[0], itr.data)

    def test_data_node_data_begining(self):
        """TEST_4A. le sommet d’une pile sur laquelle on vient d’empiler un élément e est e."""

        data_list = [22, 33, 44]
        check_list = []
        for i in data_list:
            self.test_list.insert_at_begining(i)
            check_list.append(bool(self.test_list.head.data == i))
        self.assertTrue(all(check_list))

    def test_data_node_data_end(self):
        """TEST_4B. le sommet d’une pile sur laquelle on vient d’empiler un élément e est e."""

        data_list = [22, 33, 44, 32]
        check_list = []
        for i in data_list:
            self.test_list.insert_at_end(i)
            itr = self.test_list.head
            while itr.next:
                itr = itr.next
            check_list.append(bool(itr.data == i))            
        self.assertTrue(all(check_list))    

if __name__ == '__main__':
    unittest.main()

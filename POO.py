#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Creation and manipulation of a Linked list from a txt file.
"""

__author__ = ' Gabriel Morales'

import argparse
import re



def create_parser():
    """ Declares new parser and adds parser arguments """

    program_description = "Creation and manipulation of a Linked list from a txr file"
    parser = argparse.ArgumentParser(
        add_help=True, description=program_description)
    parser.add_argument('-i', '--inputfile', type=str)
    parser.add_argument('--order', dest='order', action='store_true')
    parser.add_argument('--no-order', dest='order', action='store_false')
    parser.set_defaults(order=False)
    return parser


class Node:
    """ Creation of object Node """

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next


class LinkedList:
    """ Creation of object LinkedList and all the methods asociated """

    def __init__(self):
        self.head = None


    def insert_at_begining(self, data):
        """ Allows to insert a value at the begining of the Linkedlist """

        node = Node(data, self.head)
        self.head = node


    def print(self):
        """ Allows to print all the values of the Linkedlist """

        if self.head is None:
            print("Linked list is empty")
            return
        itr = self.head
        listr = ""
        while itr:
            listr += str(itr.data) + '-->'
            itr = itr.next
        print(listr)


    def insert_at_end(self, data):
        """ Allows to insert a value at the end of the Linkedlist """

        if self.head is None:
            self.head = Node(data, None)
            return
        itr = self.head
        while itr.next:
            itr = itr.next
        itr.next = Node(data, None)


    def insert_values(self, data_list):
        """ Allows to insert a list of values at the end of the Linkedlist """

        self.head = None
        for data in data_list:
            self.insert_at_end(data)

    def insert_values2(self, data_list):
        """ Allows to insert a list of values at the begining of the Linkedlist """

        self.head = None
        for data in data_list:
            self.insert_at_begining(data)           

    def get_lenght(self):
        """ Allows to recover the length of the Linkedlist """

        count = 0
        itr = self.head
        while itr:
            count += 1
            itr = itr.next
        return count


    def remove_at(self, index):
        """ Allows to remove a value at determined index of the Linkedlist """

        if int(index) < 0 or int(index) >= self.get_lenght():
            raise Exception("nor valid index")
        if index == 0:
            self.head == self.head.next
            return
        count = 0
        itr = self.head
        while itr:
            if count == int(index) - 1:
                itr.next = itr.next.next
                break
            itr = itr.next
            count += 1


    def insert_at(self, index, data):
        """ Allows to insert a value at determined index of the Linkedlist """

        if index < 0 or index >= self.get_lenght():
            raise Exception("nor valid index")
        if index == 0:
            self.insert_at_begining(data)
            return
        count = 0
        itr = self.head
        while itr:
            if count == index - 1:
                node = Node(data, itr.next)
                itr.next = node
                break
            itr = itr.next
            count += 1


    def insert_order(self, data):
        """ Allows to insert a list of values in order to the Linkedlist 
        / TODO CHECK THIS METHODS, IS NOT WORKING PROPERLY"""
        
        for number in data:
            if self.head is None:
                self.head = Node(int(number))
            elif int(number) < self.head.data:
                node = Node(int(number))
                node.next = self.head
                self.head = node
            else:
                node = Node(int(number))
                itr = self.head
                while itr.next is not None and itr.data < int(number):
                    itr = itr.next
                node.next = itr.next
                itr.next = node
            self.print()


    def get_response_modify(self):
        """ Allows to recover the users reponse for modifing the linked list """

        response = None
        response = input("Would you like to modify the Linked List?(Y/N)")
        if response not in ("n", "y", "N", "Y"):
            print("Please enter 'y' or 'n' \n")
            response = self.get_response_modify()
        return response


    def get_response_action(self):
        """ Allows to recover the users reponse for the type of modification at the  linked list """

        response2 = None
        response2 = input(
            "What would you like to do? (INSERT/REMOVE/GET LENGTH):")
        if response2 not in ("INSERT", "insert", "remove", "REMOVE", "GET LENGTH", "get length"):
            print("Please enter a valid option (INSERT/REMOVE/GET LENGTH) \n")
            response2 = self.get_response_action()
        return response2


    def response_insert(self):
        """ Allows to recover the users reponse for type of insertion to  the linked list """
        response_insert = None
        response_insert = input("which method would you like to use? (BEGINING/END/INDEX)")
        if response_insert not in ("BEGINING", "begining", "END", "end", "INDEX", "index"):
            print("Please enter a valid option (BEGINING/END/INDEX)")
            response_insert = self.response_insert()
        return response_insert


    def get_response_value(self):
        """ Allows to recover the users reponse for the value to add tolinked list """
        response_value = None
        response_value = input("which value would you like to insert?")
        if response_value.lstrip('-').isdigit() is False:
            response_value = self.get_response_value()
        return response_value


    def response_index(self):
        """ Allows to recover the users reponse for the index to which the values is going to be added to the linked list """
        response_index = None
        response_index = input("which index?")
        if response_index.isnumeric() is False:
            response_index = self.response_index()
        return response_index


    def users_choice(self, response_modify):
        """ decision tree to for the users input for modifing the linked list """

        if response_modify not in ("y", "Y"):
            print("End of the program, thanks for using it")
        else:
            response_action = self.get_response_action()
            if response_action in ("insert", "INSERT"):
                response_insert = self.response_insert()
                if response_insert in ("BEGINING", "begining"):
                    response_value = self.get_response_value()
                    self.insert_at_begining(response_value)
                    self.print()
                elif response_insert in ("END", "end"):
                    response_value = self.get_response_value()
                    self.insert_at_end(response_value)
                    self.print()
                elif response_insert in ("INDEX", "index"):
                    response_value = self.get_response_value()
                    response_index = self.response_index()
                    self.insert_at(response_index, response_value)
                    self.print()
            elif response_action in ("remove", "REMOVE"):
                response_index = self.response_index()
                self.remove_at(response_index)
                self.print()
            elif response_action in ("get length", "GET LENGTH"):
                print("the length of the Linked list is : " +
                      str(self.get_lenght()))
        response_modify = self.get_response_modify()
        if response_modify not in ("y", "Y"):
            print("End of the program, thanks for using it")
        else:
            self.users_choice(response_modify)


def main():
    """ Main function where all the methods are called"""

    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    if args["inputfile"] != None:
        with open(args['inputfile']) as reading_input_file:
            ll = LinkedList()
            if args["order"] is True:
                for line in reading_input_file:
                    numeric_array = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                    ll.insert_order(numeric_array)
                ll.print()
                response_modify = ll.get_response_modify()
                ll.users_choice(response_modify)
            else:
                for line in reading_input_file:
                    numeric_array = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                    ll.insert_values(numeric_array)
                    ll.print()
                    response_modify = ll.get_response_modify()
                    ll.users_choice(response_modify)


if __name__ == "__main__":
    main()
